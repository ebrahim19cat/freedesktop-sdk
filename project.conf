name: freedesktop-sdk

min-version: 2.0

aliases:
  (@): include/_private/aliases.yml

(@):
- include/_private/mirrors.yml
- include/runtime.yml

element-path: elements

fatal-warnings:
- overlaps
- unaliased-url

variables:
  (@):
  - include/_private/arch.yml
  - include/repo_branches.yml

  branch: '%{freedesktop-sdk-flatpak-branch}'
  branch-extra: '%{freedesktop-sdk-flatpak-branch-extra}'
  snap-branch: '%{freedesktop-sdk-snap-branch}'

  build-root: '/buildstream-build'
  source-date-epoch: '1320937200'

environment:
  (@): include/_private/environment.yml

  SOURCE_DATE_EPOCH: '%{source-date-epoch}'

  G_SLICE: always-malloc

environment-nocache:
  - G_SLICE

split-rules:
  (@): include/_private/split-rules.yml

plugins:
  - origin: local
    path: plugins/sources
    sources:
    - ostree_mirror

  - origin: local
    path: plugins/elements
    elements:
    - collect_initial_scripts

  - origin: junction
    junction: plugins/buildstream-plugins.bst
    elements:
    - autotools
    - cmake
    - make
    - meson
    - setuptools
    sources:
    - cargo
    - git
    - patch

  - origin: junction
    junction: plugins/bst-plugins-experimental.bst
    elements:
    - check_forbidden
    - collect_integration
    - collect_manifest
    - flatpak_image
    - flatpak_repo
    - makemaker
    - modulebuild
    - pyproject
    - snap_image
    sources:
    - cpan
    - git_tag
    - git_module
    - ostree
    - patch_queue
    - pypi
    - zip

options:
  bootstrap_build_arch:
    type: arch
    description: Architecture
    variable: bootstrap_build_arch
    values:
    - arm
    - aarch64
    - i686
    - x86_64
    - ppc64le

  target_arch:
    type: arch
    description: Architecture
    variable: target_arch
    values:
    - arm
    - aarch64
    - i686
    - x86_64
    - ppc64le
    - ppc64
    - riscv64

  snap_grade:
    type: enum
    description: Snap grade level (devel or stable)
    variable: snap_grade
    default: devel
    values:
    - devel
    - stable

artifacts:
- url: https://cache.freedesktop-sdk.io:11001

source-caches:
- url: https://cache.freedesktop-sdk.io:11001

sandbox:
  build-arch: '%{target_arch}'

elements:
  autotools:
    (@): include/_private/autotools-conf.yml
  meson:
    (@): include/_private/meson-conf.yml
  makemaker:
    (@): include/_private/makemaker.yml

sources:
  git_tag:
    (@): include/_private/git_tag-conf.yml
  pypi:
    (@): include/_private/pypi.yml

junctions:
  # We can declare these junctions as "internal" because we only use them for build dependencies
  # Therefore we know they can't collide with elements in dependant projects.
  internal:
  - abi/freedesktop-sdk.bst
  - cross-compilers/freedesktop-sdk-aarch64.bst
  - cross-compilers/freedesktop-sdk-arm.bst
  - cross-compilers/freedesktop-sdk-i686.bst
  - cross-compilers/freedesktop-sdk-riscv64.bst
  - plugins/buildstream-plugins.bst
  - plugins/bst-plugins-experimental.bst
